<?php
/**
 * Created by Stu.
 * User: stu
 * Date: 2019-01-26
 * Time: 12:50
 */

namespace App\Library;

class ProcessingGateway {

    private $gateway;
    private $wording;
    private $vars;

    public function __construct()
    {
        $this->wording = new Wording();
        $this->vars = array();
    }

    public function moProcessing($datamo=array())
    {
        $this->vars['aggregator'] = $datamo->aggregatorid;
        $this->vars['telco'] = $datamo->telcoid;
        $this->vars['msisdn'] = str_replace('+', '', $datamo->msisdn);
        $this->vars['trx_date'] = date("Y-m-d H:i:s");
        $this->vars['sms'] = strtolower($datamo->msg);
        $this->vars['substype'] = (isset($datamo->substype)) ? $datamo->substype : '';
        $this->vars['channel']  = $datamo->channel ? $datamo->channel : 'sms';
        if (stripos($datamo->sms, 'UMB') !== false) {
            $this->vars['channel'] = 'umb';
        }
        if (preg_match(config('gateway.wap_flag_regex'), $datamo->sms)) {
            $this->vars['channel'] = 'wap';
        }
        $this->vars['session_id'] = date("YmdHis") . $datamo->msisdn . rand();
        $this->vars['shortcode'] = $datamo->from;
        $this->vars['trx_id'] = (isset($datamo->transid)) ? $datamo->transid : '';
        $data_mo['spool'] = config('gateway.mo_spool');
        $data_mo['mo'] = $this->vars;
        $this->gateway->CreateMO($data_mo);
    }

    public function mtProcessing($datamt=array())
    {

    }
}