<?php
/**
 * Created by Stu.
 * User: mmdc
 * Date: 2019-01-15
 * Time: 00:16
 */

namespace App\Library;
use Illuminate\Support\Facades\Redis;

class GatewayCommon {

    public function CreateMO($data=array())
    {
        $data['mo']['created_at'] = date('Y-m-d H:i:s');
        $datagram = json_encode($data['mo']);
        $spool = $data['spool'];
        $queue_id = 'mo_queue_' . $spool;
        srand((double)microtime()*1000000);
        shuffle($spool);
        if (false === Redis::rpush($queue_id, $datagram)) {
            error_log('MOSPOOL[' . $spool[0] . ']: ' . $datagram);
            $spool_file = sprintf('%s/%s.sms', config('gateway.mo_path'), $data['mo']['session_id']);
            $writefile['file_path'] = $spool_file;
            $writefile['file_content'] = $datagram;
            $this->WriteQfile($writefile);
            return;
        }
        error_log('MOSPOOL[' . $spool[0] . ']: ' . $datagram);
    }

    public function CreateDN($data=array())
    {
        $data['dn']['created_at'] = date('Y-m-d H:i:s');
        $datagram = json_encode($data['dn']);
        $queue_id = 'dn_queue_' . $data['spool'];
        if (false === Redis::rpush($queue_id, $datagram)) {
            error_log('DNSPOOL[' . $data['spool'][0] . ']: ' . $datagram);
            $spool_file = sprintf('%s/%s.sms', config('gateway.dn_path'), $data['dn']['session_id']);
            $writefile['file_path'] = $spool_file;
            $writefile['file_content'] = $datagram;
            $this->WriteQfile($writefile);
            return;
        }
        error_log('DNSPOOL[' . $data['spool'][0] . ']: ' . $datagram);
        return;
    }

    public function CreateSQL($data=array()) {

        global $config;
        $datagram = json_encode(array(
            'created_at' => date('Y-m-d H:i:s'),
            'session_id' => $data['session_id'],
            'sql_type' => $data['sql_type'],
            'sql_data' => $data['sql_data'],
        ));

        $spool = $config['sql_spool'][$data['sql_type']];
        srand((double)microtime()*1000000);
        shuffle($spool);
        if (false === Redis::rpush('sql_queue_' . $spool[0], $datagram)) {
            $spool_file = sprintf('%s/%s-%s.sql', $config['sql_path'], $spool[0], $data['session_id']);
            $writefile['file_path'] = $spool_file;
            $writefile['file_content'] = $data['sql_data'];
            $this->WriteQfile($writefile);
        }
        return true;
    }

    public function PrintA($msg) {
        printf("%s [%s] %s%s", date('Y-m-d H:i:s'), number_format(memory_get_usage()), $msg, PHP_EOL);
    }

    public function WriteQfile($data=array()) {
        $fp = @fopen($data['file_path'], 'w');
        if ($fp) {
            fputs($fp, $data['file_content']);
            fclose($fp);
            $this->PrintA("Write queue file in: " . $data['file_path']);
        }
    }

    public function TestFunction() {
        return "This is a Test Function";
    }

}


