<?php

namespace App\Http\Middleware;

use App\Library\Wording;
use Closure;

class RequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $wording = new Wording();
        if (!in_array($request->route('telcoid'), config('gateway.telcoid')) && !in_array($request->route('aggregatorid'), config('gateway.aggregator'))) {
            return $wording->response['error']['invalid_parameter'];
        }
        if (strtolower($request->segment(1)) == 'mo') {
            // Rules for MO Request
            if (!$request->has(config('gateway.request_param')[$request->route('telcoid')])) {
                return $wording->response['error']['missing_parameters'];
            }
        } else {
            // Rules for DN Request
        }

        return $next($request);
    }
}
