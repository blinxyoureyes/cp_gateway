<?php
/**
 * Created by Stu.
 * User: mmdc
 * Date: 2019-01-21
 * Time: 22:13
 */

namespace App\Http\Controllers\Receiver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\GatewayCommon;
use App\Library\Wording;

class DnController extends Controller
{
    private $gateway;
    private $wording;
    private $vars;

    public function __construct()
    {
        $this->gateway = new GatewayCommon();
        $this->wording = new Wording();
        $this->vars = array();
    }

    public function dn(Request $request,$aggregatorid, $telcoid)
    {
        $this->vars['aggregator'] = $aggregatorid;
        $this->vars['telco'] = $telcoid;
        $this->vars['mtid'] = $request->mtid;
        if ($request->moid) {
            $this->vars['session_id'] = $request->moid;
        }
        $this->vars['delivery_date'] = date('Y-m-d H:i:s');
        $this->vars['msisdn'] = $request->msisdn;
        $this->vars['delivery_status'] = 'REJECTED';
        $this->vars['delivery_message'] = config('gateway.response_code')[$telcoid]['MOB_ACK'][$request->status];
        $this->vars['delivery_code'] = $request->status;
        if (in_array(strtoupper($request->status), config('gateway.delivery_status')[$telcoid])) {
            $this->vars['delivery_status'] = 'RECEIVED';
            $this->vars['delivery_code'] = '000';
        }
        $this->vars['snot_trx'] = false;
        $data_dn['spool'] = config('gateway.dn_spool');
        $data_dn['dn'] = $this->vars;
        $this->gateway->CreateDN($data_dn);
        return 'ok';
    }
}
