<?php
/**
 * Created by Stu.
 * User: mmdc
 * Date: 2019-01-23
 * Time: 22:51
 */

namespace App\Http\Controllers\Receiver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\GatewayCommon;
use App\Library\Wording;

class MtController extends Controller
{
    private $gateway;
    private $wording;
    private $vars;

    public function __construct()
    {
        $this->gateway = new GatewayCommon();
        $this->wording = new Wording();
        $this->vars = array();
    }
}