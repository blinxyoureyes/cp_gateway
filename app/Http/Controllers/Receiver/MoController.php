<?php
/**
 * Created by Stu.
 * Date: 2019-01-13
 * Time: 21:50
 */

namespace App\Http\Controllers\Receiver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\ProcessingGateway;
use App\Library\Wording;

class MoController extends Controller
{
    private $processing;
    private $wording;

    public function __construct()
    {
        $this->processing = new ProcessingGateway();
        $this->wording = new Wording();
    }

    public function mo(Request $request,$aggregatorid, $telcoid)
    {
        $request->aggregatorid = $aggregatorid;
        $request->telcoid = $telcoid;
        $this->processing->moProcessing($request);

        return '-1';
    }
}
