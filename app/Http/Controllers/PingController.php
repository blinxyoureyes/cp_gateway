<?php
/**
 * Created by Stu.
 * Date: 2019-01-09
 * Time: 23:02
 */

namespace App\Http\Controllers;

class PingController extends Controller
{
    public function ping()
    {
        return "pong";
    }
}
