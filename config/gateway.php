<?php
/**
 * Created by Stu.
 * User: mmdc
 * Date: 2019-01-14
 * Time: 22:50
 */

return [
    'aggregator' => array('mks','genflix'),
    'publisher_channel' => env('PUBLISHER_CHANNEL', ''),
    'base_path_logs' => '/logs',
    'base_path_spool' => '/spool',
    'mo_spool' => array(1, 2, 3),
    'dn_spool' => 3,
    'mo_path' => config('gateway.base_path_spool') . '/spoolmo',
    'mt_path' => config('gateway.base_path_spool') . '/spoolmt',
    'dn_path' => config('gateway.base_path_spool') . '/spooldn',
    'snot_path' => config('gateway.base_path_spool') . '/spoolsnot',
    'notif_path' => config('gateway.base_path_spool') . '/spoolnotif',
    'sql_path' => config('gateway.base_path_spool') . '/spoolsql',
    'sql_spool' => array(
        'mo','mt','dn'
    ),
    'telcoid' => array('xl','isat','hutch','smartfren'),
    'request_param' => array(
        'xl' => array('msisdn', 'msg', 'substype', 'channel', 'from', 'transid'),
        'isat' => array(),
        'hutch' => array(),
        'smartfren' => array()
    ),
    'wap_flag_regex' => '/ff(.*)dn/',
    'delivery_status' => array(
        'xl' => array('DELIVRD')
    ),
    'response_code' => array(
        'xl' => array(
            'SMSC_ACK' => array(
                '0' => 'Accepted for delivery',
                '1' => '1',
                '2' => '2',
                '3' => 'Queued',
                'OK' => array('0','3')
            ),
            'MOB_ACK' => array(
                '0' => 'Received by handphone',
                'DELIVRD' => 'Received by handphone',
                '000' => 'Received by handphone',
                '1284' => 'known error code return from SMSC (actually the MT is delivered to end user)',
                '0505' => 'MO/MT reach max limit',
                '2208' => 'linkID authenticate failed (DM send wrong linkID)',
                '2100' => 'service is not exist (DM send wrong serviceID or service information)',
                '3306' => 'charging failed (charging failed from billing)',
                '3309' => 'Invalid product (DM send invalid product ID or information)',
                '3101' => 'Insufficient balance',
                '1100' => 'wrong service id',
                '1001' => 'User doesn’t exist',
                '1004' => 'User is in blacklist',
                '3103' => 'Charging failed',
                '3320' => 'Subscriber account error (return from billing)',
                'OK' => array ('000','0')
            )
        )
    )
];
