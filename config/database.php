<?php
/**
 * Created by Stu.
 * User: mmdc
 * Date: 2019-01-14
 * Time: 08:38
 */

return  [
    'default' => env('DB_CONNECTION','mysql'),
    'migrations' => 'migrations',
    'connections' => [
        'gateway' => [
            'driver'    => 'mysql',
            'host'      => env('DB_BILLING_HOST', 'localhost'),
            'database'  => env('DB_BILLING_NAME', ''),
            'username'  => env('DB_BILLING_USER', ''),
            'password'  => env('DB_BILLING_PASS', ''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        'msap' => [
            'driver'    => 'mysql',
            'host'      => env('DB_MSAP_HOST', 'localhost'),
            'database'  => env('DB_MSAP_NAME', ''),
            'username'  => env('DB_MSAP_USER', ''),
            'password'  => env('DB_MSAP_PASS', ''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        'mongodb' => array(
            'driver'   => 'mongodb',
            'host'     => env('MONGODB_HOST', 'localhost'),
            'port'     => env('MONGODB_PORT', 27017),
            'username' => env('MONGODB_USERNAME', ''),
            'password' => env('MONGODB_PASSWORD', ''),
            'database' => env('MONGODB_DATABASE', ''),
            'options' => array(
                'db' => env('admin', '') //Sets the auth DB
            )
        ),
    ],
    'redis' => [
        'client' => 'predis',
        'default' => [
            'host' => env('REDIS_HOST', 'localhost'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],
    ],
];
